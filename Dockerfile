FROM golang:1.21.5-bookworm as build

WORKDIR /build/midpass

COPY . .

RUN CGO_ENABLED=0 go build -o /midpass .

FROM gcr.io/distroless/static-debian12

COPY --from=build /midpass /midpass

ENTRYPOINT ["/midpass"]
