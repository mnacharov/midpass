package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/net/html"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Appointment struct {
	Id           string `json:"Id"`
	CanConfirm   bool   `json:"CanConfirm"`
	PlaceInQueue int    `json:"PlaceInQueue"`
	FullName     string `json:"FullName"`
}

type AppointmentResponse struct {
	Items []Appointment `json:"Items"`
	Count int           `json:"Count"`
}

type ConfirmAppointments struct {
	IsSuccessful bool    `json:"IsSuccessful"`
	ErrorMessage *string `json:"ErrorMessage"`
}

const (
	CommandAppointment = "appointment"
)

type Command struct {
	ChatID int64
	Name   string
	Params []string
}

func doRequest(client *http.Client, method string, url string, formData string, refererUrl string, isJson bool) ([]byte, error) {
	var err error
	var req *http.Request
	if formData != "" {
		req, err = http.NewRequest(method, url, strings.NewReader(formData))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
	} else {
		req, err = http.NewRequest(method, url, nil)
	}
	if err != nil {
		log.Println("Error creating request:", err)
		return nil, err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:120.0) Gecko/20100101 Firefox/120.0")
	if refererUrl != "" {
		req.Header.Set("Referer", refererUrl)
	}
	if isJson {
		req.Header.Set("Accept", "application/json, text/javascript, */*; q=0.01")
		req.Header.Set("X-Requested-With", "XMLHttpRequest")
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error downloading webpage:", err)
		return nil, err
	}
	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error reading the response body:", err)
		return nil, err
	}
	return respBytes, nil
}

func collectH1Text(n *html.Node, buf *bytes.Buffer, isH1 bool) {
	if isH1 && n.Type == html.TextNode {
		buf.WriteString(n.Data)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		collectH1Text(c, buf, isH1 || n.Data == "h1")
	}
}

func checkHtml(respBytes []byte) error {
	if len(respBytes) == 0 {
		return errors.New("response is empty")
	}
	if strings.Contains(string(respBytes), "I_think_you_are_bot") {
		doc, err := html.Parse(bytes.NewReader(respBytes))
		if err != nil {
			return errors.New("I_think_you_are_bot")
		}
		text := &bytes.Buffer{}
		collectH1Text(doc, text, false)
		return errors.New(text.String())
	}
	return nil
}

func solveCapcha(bot *tgbotapi.BotAPI, chatID int64, capchaImage []byte) (string, error) {
	msg := tgbotapi.NewPhoto(chatID,
		tgbotapi.FileReader{
			Name:   "image.jpeg",
			Reader: bytes.NewReader(capchaImage),
		})
	messageSent, err := bot.Send(msg)
	if err != nil {
		return "", fmt.Errorf("failed to send photo: %v", err)
	}
	log.Printf("Captcha sent to chat %d, with ID: %d\n", chatID, messageSent.MessageID)

	config := tgbotapi.NewUpdate(0)
	config.Timeout = 60

	for {
		updates, err := bot.GetUpdates(config)
		if err != nil {
			log.Println(err)
			log.Println("Failed to get updates, retrying in 3 seconds...")
			time.Sleep(time.Second * 3)
			continue
		}

		for _, update := range updates {
			if update.UpdateID >= config.Offset {
				config.Offset = update.UpdateID + 1
			}
			if update.Message == nil || update.Message.ReplyToMessage == nil || update.Message.ReplyToMessage.MessageID != messageSent.MessageID {
				continue // Skip non-reply messages or messages not related to our sent photo
			}
			return strings.TrimSpace(update.Message.Text), nil
		}
		time.Sleep(time.Second)
	}
}

func checkAndConfirmAppointment(bot *tgbotapi.BotAPI, chatID int64, email string, password string) error {
	// Step 1: Auth
	// 1.1 init cookies
	jar, err := cookiejar.New(nil)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("Error make cookie jar: %s", err.Error())))
		return err
	}
	client := &http.Client{
		Jar: jar,
	}
	respBytes, err := doRequest(client, http.MethodGet, "https://q.midpass.ru/", "", "", false)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}
	err = checkHtml(respBytes)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}
	// 1.2 get capcha
	respBytes, err = doRequest(client, http.MethodGet, fmt.Sprintf("https://q.midpass.ru/ru/Account/CaptchaImage?%d=", time.Now().Unix()), "", "", false)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}

	// 1.3 solve capcha
	capcha, err := solveCapcha(bot, chatID, respBytes)
	if capcha == "" || err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("failed to solve capcha: %s", err)))
		return err
	}

	// 1.4 do login request
	formData := url.Values{
		"NeedShowBlockWithServiceProviderAndCountry": {"True"},
		"CountryId":            {"88655495-3b8c-f56d-5337-0f2743a7bfed"},
		"ServiceProviderId":    {"b8af6319-9d8d-5bd9-f896-edb8b97362d0"},
		"Email":                {email},
		"g-recaptcha-response": {},
		"Captcha":              {capcha},
		"Password":             {password},
	}
	respBytes, err = doRequest(client, http.MethodPost, "https://q.midpass.ru/ru/Account/DoPrivatePersonLogOn", formData.Encode(), "https://q.midpass.ru/", false)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}
	err = checkHtml(respBytes)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}

	// Step 2. Appointments
	// 2.1 get appointments list
	respBytes, err = doRequest(client, http.MethodPost, "https://q.midpass.ru/ru/Appointments/FindWaitingAppointments", "begin=0&end=10", "https://q.midpass.ru/ru/Appointments/WaitingList", true)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}

	var appointmentResponse AppointmentResponse
	err = json.Unmarshal(respBytes, &appointmentResponse)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("Error decoding JSON: %s", err)))
		return err
	}
	bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("%+v\n", appointmentResponse)))

	// 2.2 find appointments to confirm
	var ids []string
	for _, app := range appointmentResponse.Items {
		if app.CanConfirm {
			ids = append(ids, app.Id)
		}
	}
	if len(ids) == 0 {
		bot.Send(tgbotapi.NewMessage(chatID, "nothing to confirm"))
		return err
	}
	// 2.3 get capcha
	respBytes, err = doRequest(client, http.MethodGet, fmt.Sprintf("https://q.midpass.ru/ru/Account/CaptchaImage?%d=", time.Now().Unix()), "", "https://q.midpass.ru/ru/Appointments/WaitingList", false)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, err.Error()))
		return err
	}

	// 2.4 solve capcha
	capcha, err = solveCapcha(bot, chatID, respBytes)
	if capcha == "" || err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("failed to solve capcha: %s", err)))
		return err
	}

	// 2.5 confirm appointments
	formData = url.Values{
		"captcha": {capcha},
		"ids":     ids,
	}
	respBytes, err = doRequest(client, http.MethodPost, "https://q.midpass.ru/ru/Appointments/ConfirmWaitingAppointments", formData.Encode(), "https://q.midpass.ru/ru/Appointments/WaitingList", true)

	var confirmAppointments ConfirmAppointments
	err = json.Unmarshal(respBytes, &confirmAppointments)
	if err != nil {
		bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("Error decoding JSON: %s", err)))
		return err
	}
	bot.Send(tgbotapi.NewMessage(chatID, fmt.Sprintf("%+v\n", confirmAppointments)))
	return nil
}

func waitCommand(bot *tgbotapi.BotAPI) (*Command, error) {
	config := tgbotapi.NewUpdate(0)
	config.Timeout = 60

	for {
		updates, err := bot.GetUpdates(config)
		if err != nil {
			log.Println(err)
			log.Println("Failed to get updates, retrying in 3 seconds...")
			time.Sleep(time.Second * 3)
			continue
		}
		for _, update := range updates {
			if update.UpdateID >= config.Offset {
				config.Offset = update.UpdateID + 1
			}
			// Check if the update is a new message and has text content
			if update.Message == nil || !strings.HasPrefix(update.Message.Text, "/") {
				continue // Skip not command messages
			}
			if strings.HasPrefix(update.Message.Text, fmt.Sprintf("/%s", CommandAppointment)) {
				v := strings.Split(update.Message.Text, " ")
				if len(v) < 3 {
					continue
				}
				return &Command{update.Message.Chat.ID, CommandAppointment, v[1:3]}, nil
			}
		}
		time.Sleep(time.Second)
	}
	return nil, fmt.Errorf("NoCommand?")
}

func main() {
	botToken := os.Getenv("TELEGRAM_BOT_TOKEN")
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Fatalln("failed to create bot:", err)
	}
	for {
		cmd, err := waitCommand(bot)
		if err != nil {
			log.Fatalln("failed wait command:", err)
			bot, err = tgbotapi.NewBotAPI(botToken)
			if err != nil {
				log.Fatalln("failed to re-create bot:", err)
			}
		}
		if cmd.Name == CommandAppointment {
			err = checkAndConfirmAppointment(bot, cmd.ChatID, cmd.Params[0], cmd.Params[1])
			if err != nil {
				bot, err = tgbotapi.NewBotAPI(botToken)
				if err != nil {
					log.Fatalln("failed to re-create bot:", err)
				}
			}
		}
		time.Sleep(5 * time.Second)
	}
}
