module gitlab.com/mnacharov/midpass

go 1.21.1

require github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1

require golang.org/x/net v0.20.0 // indirect
